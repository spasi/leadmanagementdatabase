<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
<link href="css/loginstyle.css" rel="stylesheet" type="text/css" />
<script> 
function validate()
{ 
var username = document.form.txtUserName.value; 

var password = document.form.txtPassword.value;

if (username==null || username=="")
{ 
alert("Username cannot be blank"); 
return false; 
}
else if(password==null || password=="")
{ 
alert("Password cannot be blank"); 
return false; 
} 
}
</script> 
</head>
<body >
        <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
			 <tr>
                <td class="aHeaderTd" height="75">
                    <table width="80%" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tbody><tr>
                            <td><img src="images/logo.png" alt="Cydcor">                        </td>
                            <td>&nbsp;</td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
            
            
             <tr>
                <td class="content">
                    <table class="logintab" width="450" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tbody><tr>
                            <td>
                                <div class="logintitle">
                                    <h3>Lead Management Application</h3>
                                    
                                </div>
                            </td>
                        </tr>

						<form name="form" action="LoginServlet" method="post" onsubmit="return validate()">
						<tr>
                            <td>
                                <table class="loginform" cellspacing="0" cellpadding="5" border="0">
                                    <tbody>
                                    <tr>
                                        <td class="label">User Name</td>
                                        <td>
                                            
                                            <input name="txtUserName" id="txtUserName" class="txtinput" style="width:120px;" type="text">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">Password</td>
                                        <td>
                                            
                                            <input name="txtPassword" id="txtPassword" class="txtinput" style="width:120px;" type="password">
                                        </td>
                                    </tr>
									
									<tr> 
									<td>
									<span style="color:red"><%=(request.getAttribute("errMessage") == null) ? ""
									: request.getAttribute("errMessage")%></span></td>
									</tr>
									
                                    <tr>
                                        <td rowspan="2">
                                            
                                        
                                        </td>
                                    </tr>
                                </tbody></table>
                            </td>
                        </tr>
            			<tr>
                            <td>
                                <div class="buttons">
                                    <input name="btnLogin" value="Login" id="btnLogin" class="grnbutton" type="submit">
                                     <input name="btnForgotPassword" value="Reset" id="btnLogin" class="grnbutton" type="reset">
                                    <br>
                                   
                                </div>
                            </td>
                        </tr>
                  </form>
      				
      		     	  </tbody>
             </table>
           </td>
          </tr>
            
                <tr>
                <td class="footer" height="50px;" >
                    <table width="80%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tbody><tr>
                            <td><span class="copy">�2015 All Rights Reserved.</span></td>
                            <td align="right"><span class="disclaimer">Disclaimer:</span> <span class="copy">Unauthorized use of this site is prohibited and may be subject to civil and criminal prosecution.</span></td>
                        </tr>
                    </tbody>
                    </table>
                </td>
            </tr>
			

        </table>
</body>
</html>