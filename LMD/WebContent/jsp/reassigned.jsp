<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home Page</title>
<link href="css/Homescreen.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" charset="utf-8" language="javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8" language="javascript" src="js/jquery-1.12.4.min.js"></script>
<script type="text/javascript" charset="utf-8" language="javascript" src="js/pagination.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>


        <style type="text/css">    
            .pg-normal {
                font-weight: bold;
                text-decoration: none;    
                cursor: pointer;
                color:#003B72;    
            }
            .pg-selected {
                color: red;
                font-weight: bold;        
                text-decoration: underline;
                cursor: pointer;
            }
            
             
        </style>

<script>



function DownloadleadUploadHistoryTable()
{
	var state = $(".state").val();
    alert(state);
	 $.ajax({
	        type: "POST",
	        url: "jsp/exportreport.jsp",
	        data: "state="+state,
	        cache: false,
	        success: function(response)
	        {
	            alert(response);
	        }
	    });

	 var mywindow = window.open (url,
			  "Report","location=1,status=1,scrollbars=1, width=300,height=300");
			  mywindow.moveTo(0,0);			  
}

</script>
        
<script>

function state_change()
{
	var state = $(".state").val();
	
	 $.ajax({
	        type: "POST",
	        url: "jsp/city.jsp",
	        data: "state="+state,
	        cache: false,
	        success: function(response)
	        {
	            $(".city").html(response);
	        }
	    });

}
function Statesearch()
{
	var state = $(".state").val();
	var city = $(".city").val();
	var zip = $(".zip").val();
	
	 $.ajax({
	        type: "POST",
	        url: "jsp/searchstate.jsp",
	        data: "state="+state,"zip="+zip,"city="+city,
	        cache: false,
	        success: function(response)
	        {
	            $("#checkpoint").html(response);
	        }
	    });

    
}


function Citysearch()
{
	var state = $(".state").val();
	var city = $(".city").val();
	var zip = $(".zip").val();
	
	 $.ajax({
	        type: "POST",
	        url: "jsp/searchstate.jsp",
	        data: "state="+state,"zip="+zip,"city="+city,
	        cache: false,
	        success: function(response)
	        {
	            $("#checkpoint").html(response);
	        }
	    });

}

function Zipsearch()
{
	var state = $(".state").val();
	var city = $(".city").val();
	var zip = $(".zip").val();
	
	 $.ajax({
	        type: "POST",
	        url: "jsp/searchstate.jsp",
	        data: "state="+state,"zip="+zip,"city="+city,
	        cache: false,
	        success: function(response)
	        {
	            $("#checkpoint").html(response);
	        }
	    });

}



function city_change()
{
	var city = $(".city").val();
    	
	 $.ajax({
	        type: "POST",
	        url: "jsp/zip.jsp",
	        data: "city="+city,
	        cache: false,
	        success: function(response)
		      {
		          $(".zip").html(response);
		      }
		   });
	
}

function changeICLOwner()
{
	 var campaignSeq = $(".campaignSeq").val();
	 
	 $.ajax({
	        type: "POST",
	        url: "jsp/owner.jsp",
	        data: "campaignSeq="+campaignSeq,
	        cache: false,
	        success: function(response)
		      {
		          $(".ownerlist").html(response);
		      }
		   });
	
}

function changeCamptable()
{
	var campaignSeq = $(".campaignSeq").val();
	var ownerSeqtable = $(".ownerlist").val();
	
	 
	 $.ajax({
	        type: "POST",
	        url: "jsp/searchstate.jsp",
	        data: "campaignSeq="+campaignSeq,"ownerSeqtable="+ownerSeqtable,
	        cache: false,
	        success: function(response)
		      {
		            $("#checkpoint").html(response);
		      }
		   });
	
}

function changeOwnerICLtable()
{
	var campaignSeq = $(".campaignSeq").val();
	var ownerSeqtable = $(".ownerlist").val();
		    
	 
	 $.ajax({
	        type: "POST",
	        url: "jsp/searchstate.jsp",
	        data: "campaignSeq="+campaignSeq,"ownerSeqtable="+ownerSeqtable,
	        cache: false,
	        success: function(response)
		      {
		            $("#checkpoint").html(response);
		      }
		   });
	
}



function changeICLOwners()
{
	 var campaignSeq = $(".campaignSeq1").val();
	 
	 $.ajax({
	        type: "POST",
	        url: "jsp/owner.jsp",
	        data: "campaignSeq="+campaignSeq,
	        cache: false,
	        success: function(response)
		      {
		          $(".ownernames").html(response);
		      }
		   });
	
}


</script>


<script>
function handleSelectChangescamp()
{
	var campaignSeq = document.getElementById("campaignSeq1").value;	
	
	if(campaignSeq == "")
	{	
		alert("Please select the campaign ");	
	}
	
}

function handleSelectChangesvalues()
{
	//assigned
	var ownername = document.getElementById("campaignSeq").value;
	
	//alert(ownername);
	if(ownername == "")
	{	
		document.getElementById("assigned").style.display = 'none';
	}
	else
	{	
		document.getElementById("assigned").style.display = 'block';   	
	}
}


</script>

<script type="text/javascript">
    function checkAll(checkId){
        var inputs = document.getElementsByTagName("input");
        for (var i = 0; i < inputs.length; i++) { 
            if (inputs[i].type == "checkbox" && inputs[i].id == checkId) { 
                if(inputs[i].checked == true) {
                    inputs[i].checked = false ;
                } else if (inputs[i].checked == false ) {
                    inputs[i].checked = true ;
                }
            }  
        }  
    }
</script>

<script>
function openCity(cityName,elmnt,color) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.backgroundColor = "";
    }
    document.getElementById(cityName).style.display = "block";
    elmnt.style.backgroundColor = color;

}
// Get the element with id="defaultOpen" and click on it
//document.getElementById("defaultOpen").click();
</script>

</head>
<body style="background-color:#D3D3D3;">
 <table class="aMainTable" cellspacing="0" cellpadding="0" align="center">
    <tbody><tr>
      <td class="aHeaderTd"><table class="aLogoTable">
		  <tbody><tr>
			<td class="aLogoTd"><div class="aLogo"><img src="images/logo.png" alt="cydcor" width="226" height="74" align="left"></div></td>
			<td style="vertical-align: bottom;" align="right"><table cellspacing="0" cellpadding="0" border="0">
			  <tbody><tr>
				<td class="aLoggedUser" align="center"><span class="red-txt">Welcome to the Lead Management Application</span></td>
			  </tr>
			  <tr>
				<td style="color:#003B72;font-weight:BOLD;" align="center"><%= request.getAttribute("userName") %>    
				&nbsp;&nbsp;&nbsp;&nbsp;<a href="LogoutServlet">[logout]</a>
				&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onclick="#">Support</a>
				</td>
				
			  </tr>
			</tbody></table></td>
			<td width="25" align="right">&nbsp;</td>
		  </tr>
		  
		  <tr>
		  	<td colspan="3" width="100%">
		  		<table id="twoMenusTableId" width="100%">
		  			<tbody><tr>
		  				<td>
							<table width="100%">
		  

									<tbody><tr><td class="menubarShadow" height="35"><table width="95%" cellspacing="0" cellpadding="0" border="0" align="center">
									  <tbody><tr>
										<td align="left"><table cellspacing="0" cellpadding="0" border="0">
											<tbody><tr>
											  <td><a href="#" style="text-decoration:none;"><span target-div="#" id="mainMenu1" class="mainMenu1 activeTab">ASSIGN / REASSIGN</span></a></td>
											  <td><a href="#" style="text-decoration:none;"><span target-div="#" id="mainMenu2" class="mainMenu2" > ADDRESS VALIDATION</span></a></td>
											  <td><a href="#" style="text-decoration:none;"><span target-div="#" id="mainMenu3" class="mainMenu3"> HOLD / RELEASE LEADS</span></a></td>
											</tr>
										</tbody></table></td>
										<td align="right"><table cellspacing="0" cellpadding="0" border="0">
											<tbody><tr>
											</tr>
										</tbody></table></td>
									  </tr>
									  
									</tbody></table></td>
								  </tr>
								  <tr style="color: #FFFFFF; background-color: #003B72; padding : 5px;">
								  <!-- for sub-menus -->
									  	<td height="24" align="left">
									  	
									  		<div id="subMenuDiv1" class="subMenuDiv" style="float: left; display: block;">
										  		
											</div>
									  	</td>
									  </tr>
			  
								</tbody></table>			  	
								
										
								
										  
			  		  		</td>
			  			</tr>
			  		</tbody></table>
			  		
			  		
			  		
			  	</td>
			  </tr>
			  			  
		</tbody></table>
		
		<table style="margin-left:-17px;width: 101%;text-align: left;height:30px;">
		<tr>			
				<td style="background-color:#4c4c4c;color:#FFFFFF;font-size:15px;font-weight:bold;">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Search Options
				</td>
				
		</tr>		
		</table>	
		<br>
		<table style="margin-left:20px;margin-top:-47px;">
		<div id="London" class="tabcontent" align="left" style="margin-left:20px;">
		
		  <tr>
				<td style="color:#000000;font-weight:BOLD;">
					State:
					 <select name="state" id="state" class="state" onchange="state_change(this.value);Statesearch()">
						<option value=""> </option>
			            <c:forEach items="${list_state}" var="list">
			                <option value="${list.state}">			                    
			                    ${list.state}
			                </option>
			            </c:forEach>
			        </select>
			
				 </td>
				<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				<td style="color:#000000;font-weight:BOLD;">
					City:
					 <!-- 
					 <select class="city" name="city">
			            <c:forEach items="${list_state}" var="list">
			                <option value="${list.city}">			                    
			                    ${list.city}
			                </option>
			            </c:forEach>
			        </select>	
			          -->
					<select class="city" id="city" onchange="city_change();Citysearch()">
					 	<option selected="selected"> </option>
					</select>		     
			     </td>
			     
			
				<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				<td style="color:#000000;font-weight:BOLD;">
					Zip:
				</td>
				<td>
					 
					 <select class="zip" multiple="multiple" id="zip" onchange='Zipsearch();' style="width:60px;margin-top:22px;">
    					<option selected="selected"></option>
    				</select>
					 
					 
				 </td>
				     
			  </tr>
		
		<br>
			<table style="margin-left:20px;">
					<tr>
					<td style="color:#000000;font-weight:BOLD;">
						Campaign:
				     
				     <select id="campaignSeq" name="campaignSeq" class="campaignSeq" onchange="handleSelectChangesvalues(this.value);changeICLOwner(this.value);changeCamptable(this.value);">
			            <option value=""> </option>
			            <c:forEach items="${campaginlist}" var="clist">
			                <option value="${clist.campaigncode}">			                    
			                    ${clist.campaignname}
			                </option>
			            </c:forEach>
			        </select>
	
				     
				     </td>
			
						    
					<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					<td style="color:#000000;font-weight:BOLD;">
						Owner:
						 <select class="ownerlist" onchange="changeOwnerICLtable(this.value)">
						 	<option selected="selected"> </option>
						</select>
				     </td>
				     <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				     <td style="color:#000000;font-weight:BOLD;display:none;" id="assigned">
				     	Status:
				     	<select name="assigned" id="assigneded" onchange="handlestatus(this.value)">
	 			        <option value="Assigned">Assigned</option>
				        <option value="NEW">NEW</option>
				     </select>
				     </td> 
				     </tr>
			</table>
		<br>
										
					<div id="checkpoint" style="overflow-y:scroll;max-height: 400px;"> 
					
					</div>           
			    	<span></span>
					<div id="pageNavPosition" style="margin-left:275px;width:40%;">
					</div>
			    	
			    	
			    	
					<table style="margin-left:-17px;width: 100%;text-align: left;height:30px;">
						<tr>			
								<td style="background-color:#4c4c4c;color:#FFFFFF;font-size:15px;font-weight:bold;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Operations
							</td>
						</tr>		
					</table>	
					<br>
		
		
				<table style="margin-left:20px;">
					<tr>
						<td style="color:#000000;font-size:15px;font-weight:bold;">
						Assign to:
						</td>
				
						<td style="padding-right: 10px"></td>
						
						<td style="color:#000000;font-weight:BOLD;">
							 Campaign:
							 <select id="campaignSeq1" name="campaignSeq1" class="campaignSeq1" onchange="changeICLOwners(this.value);">
						        <option value=""> </option>
				    		        <c:forEach items="${campaginlist}" var="clist">
				            	    <option value="${clist.campaigncode}">			                    
				                	    ${clist.campaignname}
				                	</option>
				            		</c:forEach>
				    		</select>
				     	</td> 
			     		
			     		<td style="padding-right: 20px"></td>
							<td style="color:#000000;font-weight:BOLD;">
							Owner:
							 <select class="ownernames">
						 		<option selected="selected"> </option>
							</select>
				             
					        
			     			</td>
			     	</tr>
			    </table>					    
			    
    					
			

							             <table class="logintab" style="margin-left:25px;margin-top:20px;margin-bottom:20px;">
									    
			                                  <td>
			                                    <input name="btnLogin" value="ASSIGN / REASSIGN" id="btnLogin" class="grnbutton" type="submit" onclick="doSubmit();">
			                                  </td>
			                                  <td>
			                                    <input name="btnForgotPassword" value="EXPORT TO CVS" id="btnLogin" class="grnbutton" type="reset" onclick="javascript:DownloadleadUploadHistoryTable();">
			                                  </td>
			                             
						        		 </TABLE><p>
			  
			</div>
			
		</td>
    </tr>
    	<tr>
    	  <td class="footer" height="25px;" >
                    <table width="80%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tbody><tr>
                            <td><span class="copy">©2015 All Rights Reserved.</span></td>
                            <td align="right"><span class="disclaimer">Disclaimer:</span> <span class="copy">Unauthorized use of this site is prohibited and may be subject to civil and criminal prosecution.</span></td>
                        </tr>
                    </tbody>
                    </table>
                </td>
		</tr>	    
    
    </div></table>
  </tbody></table>
  
			
				
    
</body>
</html>



				
