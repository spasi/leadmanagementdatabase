<%@page import="java.sql.*" %>
<%@page import="com.cydor.util.DBConnection;" %>

<%
if(request.getParameter("campaignSeq")!=null) 
{
    String campaignSeqs = request.getParameter("campaignSeq"); //get state_id from index.jsp page with function state_change() through ajax and store in id variable.
        
    try
    {
    	Connection con = null;
    	Statement statement = null;
    	ResultSet resultSet = null;
    	      
    	con = DBConnection.createConnection(); //establishing connection
		statement = con.createStatement(); //Statement is used to write queries. Read more about it.
		resultSet = statement.executeQuery("Select Distinct IA.ICL_UNIFIED_CODE__C, IA.Name as ICL_Name FROM dev_cyd_leads.SF_CYDCOR_CAMPAIGNICLAGENTASSIGNMENT__C CIA 	JOIN dev_cyd_leads.SF_CYDCOR_CLIENTCAMPAIGN__C  CC ON CC.ID = CIA.CLIENT_CAMPAIGN__C JOIN dev_cyd_leads.SF_ICL_ACCOUNT IA ON IA.ID = CIA.ICL__C  Where ACTIVE_FLAG__C =1 and CC.CAMPAIGN_UNIFIED_CODE__C = " + "'" +campaignSeqs  + "'" + " order by ICL_Name ASC"); //Here table name is users and userName,password are columns. fetching all the records and storing in a resultSet. 
        %>        
            <option selected="selected"></option>
        <%    
        while(resultSet.next())
        {
        %> 
            <option value="<%=resultSet.getString("ICL_UNIFIED_CODE__C")%>">
                <%=resultSet.getString("ICL_Name")%>
            </option>
        <%
        }
  
  con.close(); //close connection 
    }
    catch(Exception e)
    {
        out.println(e);
    }
}
%>

