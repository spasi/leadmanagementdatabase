<%@page import="java.sql.*" %>
<%@page import="com.cydor.util.DBConnection;" %>

<%
if(request.getParameter("state")!=null && request.getParameter("city")!=null && request.getParameter("zip")!=null) 
{
    String state = request.getParameter("state"); 
    String city = request.getParameter("city"); 
	String[] zip = request.getParameterValues("zip");
 
    
    System.out.println("state" + state);
    System.out.println("city" + city);
    System.out.println("zip" + zip);
    
    try
    {
    	Connection con = null;
    	Statement statement = null;
    	ResultSet resultSet = null;
    	String res = "";
      
    	con = DBConnection.createConnection(); //establishing connection
		statement = con.createStatement(); //Statement is used to write queries. Read more about it.
		
		if(state != null && city==null && zip==null)
		{
			String searchstate = " SELECT TL.zip_code ,COUNT(QL.row_id) AS lead_count,CC.Name AS campaign_name ,IA.ICL_UNIFIED_CODE__C AS icl_code ,IA.Name AS ICL_Name FROM cydcor_leads_sat.territory_assign_summary TS INNER JOIN  cydcor_leads_sat.lkup_territory TL ON TL.Territory_id = TS.territory_id INNER JOIN cydcor_leads_stg.stg_cydcor_dbusa_bus_leads QL ON  TL.zip_code = QL.physical_addr_zip AND TL.state = QL.physical_addr_state AND TL.city = QL.physical_addr_city INNER JOIN cydcor_leads_sf.CYDCOR_CLIENTCAMPAIGN__C CC  ON TL.territory_type = CC.CAMPAIGN_TYPE__C  AND TS.campaign_id = CC.ID INNER JOIN cydcor_leads_sf.ICL_ACCOUNT IA ON IA.ICL_UNIFIED_CODE__C = TS.owner_code WHERE TS.Active = 'Y'  AND (QL.processed_status != 'INVALID'  AND  QL.processed_status != 'DUPLICATE') AND CC.ACTIVE__c =1 AND TL.zip_code IS NOT NULL and QL.physical_addr_state = " + "'" + state  + "'" + " GROUP BY TL.zip_code ,TL.state ,TL.city"; 
			resultSet = statement.executeQuery(searchstate);
			System.out.println("..........." + searchstate);    
		}
		if(state != null && city != null && zip==null)
		{
			String searchstatecity = " SELECT TL.zip_code ,COUNT(QL.row_id) AS lead_count,CC.Name AS campaign_name ,IA.ICL_UNIFIED_CODE__C AS icl_code ,IA.Name AS ICL_Name FROM cydcor_leads_sat.territory_assign_summary TS INNER JOIN  cydcor_leads_sat.lkup_territory TL ON TL.Territory_id = TS.territory_id INNER JOIN cydcor_leads_stg.stg_cydcor_dbusa_bus_leads QL ON  TL.zip_code = QL.physical_addr_zip AND TL.state = QL.physical_addr_state AND TL.city = QL.physical_addr_city INNER JOIN cydcor_leads_sf.CYDCOR_CLIENTCAMPAIGN__C CC  ON TL.territory_type = CC.CAMPAIGN_TYPE__C  AND TS.campaign_id = CC.ID INNER JOIN cydcor_leads_sf.ICL_ACCOUNT IA ON IA.ICL_UNIFIED_CODE__C = TS.owner_code WHERE TS.Active = 'Y'  AND (QL.processed_status != 'INVALID'  AND  QL.processed_status != 'DUPLICATE') AND CC.ACTIVE__c =1 AND TL.zip_code IS NOT NULL and QL.physical_addr_state = " + "'" + state  + "'" + " and and QL.physical_addr_city= " + "'" + city  + "'" + "  GROUP BY TL.zip_code ,TL.state ,TL.city"; 
			resultSet = statement.executeQuery(searchstatecity);
			System.out.println("..........." + searchstatecity);
		}
		
		if(state != null && city != null && zip!=null)
		{
			for (String i : zip) 
			{
			      res += res.isEmpty() ? i : ","+i;
			      System.out.println("..........." +res);    
			      String searchstatezip = " SELECT TL.zip_code ,COUNT(QL.row_id) AS lead_count,CC.Name AS campaign_name ,IA.ICL_UNIFIED_CODE__C AS icl_code ,IA.Name AS ICL_Name FROM cydcor_leads_sat.territory_assign_summary TS INNER JOIN  cydcor_leads_sat.lkup_territory TL ON TL.Territory_id = TS.territory_id INNER JOIN cydcor_leads_stg.stg_cydcor_dbusa_bus_leads QL ON  TL.zip_code = QL.physical_addr_zip AND TL.state = QL.physical_addr_state AND TL.city = QL.physical_addr_city INNER JOIN cydcor_leads_sf.CYDCOR_CLIENTCAMPAIGN__C CC  ON TL.territory_type = CC.CAMPAIGN_TYPE__C  AND TS.campaign_id = CC.ID INNER JOIN cydcor_leads_sf.ICL_ACCOUNT IA ON IA.ICL_UNIFIED_CODE__C = TS.owner_code WHERE TS.Active = 'Y'  AND (QL.processed_status != 'INVALID'  AND  QL.processed_status != 'DUPLICATE') AND CC.ACTIVE__c =1 AND TL.zip_code IS NOT NULL and QL.physical_addr_state = " + "'" + state  + "'" + " and and QL.physical_addr_city= " + "'" + city  + "'" + "  and QL.physical_addr_zip IN (" + res  + ") GROUP BY TL.zip_code ,TL.state ,TL.city"; 
				  resultSet = statement.executeQuery(searchstatezip);
				  System.out.println("..........." + searchstatezip);
					
			 }
			
		}
		
		
	
	%>
		
		<table id="mytable" class="crmWidth" >
		<thead>
		      <TR style="text-align:center;font-size: 13px;font-weight:Bold;padding:5px;" class="label;">
			  <th class="LastCol" width="5px;" style="color:#FFFFFF;font-weight:Bold;font-size:15px;">
			  <input type="checkbox" id="chk_new"  onclick="checkAll('chk');" >
			  Select All
			  </th>
			  <th width="10px;" style="color:#FFFFFF;font-weight:Bold;font-size:15px;">ZIP</th>
			  <th width="15px;" style="color:#FFFFFF;font-weight:Bold;font-size:15px;">Current Office</th>
			  <th width="25px;" style="color:#FFFFFF;font-weight:Bold;font-size:15px;">Lead Count</th>
			  <th width="25px;" style="color:#FFFFFF;font-weight:Bold;font-size:15px;">Campaign</th>
			  <th width="25px;" style="color:#FFFFFF;font-weight:Bold;font-size:15px;">New Office</th>
			  </TR>
		</thead>
		<%
		while(resultSet.next())
        { %>
       		 <TR style="text-align:center" class="label" >
			     <td width="5px;" style="background: none repeat scroll 0 0 #B4CE62;border-top: 1px solid #FFFFFF;color: #003B72;padding: 5px;">
	            	<input type="checkbox" id="chk" value="1" name="chk[]"  >
			     </td>
			   	
				<td style="background: none repeat scroll 0 0 #B4CE62;border-top: 1px solid #FFFFFF;color: #003B72;padding: 5px;font-size: 13px;color:##003B72;font-weight:Bold;">
	                <%= resultSet.getString("zip_code") %>
	            </td>
			   	           
			   	<TD style="background: none repeat scroll 0 0 #B4CE62;border-top: 1px solid #FFFFFF;color: #003B72;padding: 5px;font-size: 13px;color:##003B72;font-weight:Bold;">
				</TD>           
				
				 <TD style="background: none repeat scroll 0 0 #B4CE62;border-top: 1px solid #FFFFFF;color: #003B72;padding: 5px;font-size: 13px;color:##003B72;font-weight:Bold;"> 
				      <%= resultSet.getInt("row_id") %>
			     </TD>
				
				<% if(resultSet.getString("Name") != null)
			 	{ 
				 	%>
						 <TD style="background: none repeat scroll 0 0 #B4CE62;border-top: 1px solid #FFFFFF;color: #003B72;padding: 5px;font-size: 15px;color:##003B72;font-weight:Bold;">
						 <%= resultSet.getString("Name") %>
						 </TD>
				<% }
				else 
				{ %>
					<TD style="background: none repeat scroll 0 0 #B4CE62;border-top: 1px solid #FFFFFF;color: #003B72;padding: 5px;font-size: 15px;color:##003B72;font-weight:Bold;">	
				 	<select id="ownernames" name="ownernames" >
				 				<option value=""> </option>
					                <option value="90555">			                    
					                    QUILL
					                </option>
					 </select>
					 </TD>
				<% } %>
						        		
				<td style="background: none repeat scroll 0 0 #B4CE62;border-top: 1px solid #FFFFFF;color: #003B72;padding: 5px;font-size: 13px;color:##003B72;font-weight:Bold;">
						<select id="ownernames" name="ownernames" >
					            <option selected="selected"></option>
					            <option value="MOH9">
					                Gateway Sales Consulting, Inc.
					            </option>
					         
					            <option value="TXU4">
					                Legacy Business Development, Inc.
					            </option>
					         
					            <option value="KSB8">
					                LJC Business Partners, Inc.
					            </option>
					         
					            <option value="MIG5">
					                Presden Consultants, Inc.
					            </option>
					        </select>
				</td>    
			</TR>					           		
			           
      <%  } %>
  <% 
  con.close(); //close connection 
    }
    catch(Exception e)
    {
        out.println(e);
    }
}

%>



					<script type="text/javascript"><!--
						var pager = new Pager('mytable', 20);
						pager.init();
						pager.showPageNav('pager', 'pageNavPosition');
						pager.showPage(1);
					</script>
		
				    	
					
