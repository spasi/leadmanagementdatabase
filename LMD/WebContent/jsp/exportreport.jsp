<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.List" %>
<%@page import="java.sql.*" %>
<%@page import="com.cydor.util.DBConnection" %>
<%@page import="org.apache.poi.hssf.usermodel.HSSFRow" %>
<%@page import="org.apache.poi.hssf.usermodel.HSSFSheet" %>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook" %>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook" %>
<%@page import="java.io.FileWriter" %>
<%@page import="java.io.File" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.FileInputStream" %>
<%@page import="java.io.OutputStream" %>

<%
	if(request.getParameter("state")!=null ) 
	{
	
	String state = request.getParameter("state"); 
	System.out.println("state" + state);    
		   	
	Connection con = null;
	Statement statement = null;
	ResultSet resultSet = null;
	      
	con = DBConnection.createConnection(); //establishing connection
	statement = con.createStatement(); //Statement is used to write queries. Read more about it.
	
	String contpe="";
    
	File tempFile = File.createTempFile(getClass().getName(), ".xls");
    
	try
	{
		// Need to add Select query for export
        String strSimExportQuery=" SELECT PhysicalAddrZip, count(RowID) as RowID FROM dev_cyd_leads_stg.stg_quill_leads where PhysicalAddrState= " + "'" + state  + "'" + "  and PhysicalAddrZip IS NOT NULL GROUP BY PhysicalAddrZip "; 
       	resultSet = statement.executeQuery(strSimExportQuery);
       	
       	HSSFWorkbook hwb = new HSSFWorkbook();
        HSSFSheet sheet = hwb.createSheet("new sheet");
		
        HSSFRow rowhead = sheet.createRow((short) 2);
        
        rowhead.createCell((short) 0).setCellValue("Sr.No");
        rowhead.createCell((short) 1).setCellValue("PhysicalAddrZip");
        rowhead.createCell((short) 2).setCellValue("RowID");
        
        int index = 3;
        int sno = 0;
        String name = "";
        
        	while(resultSet.next())
        	{
	        	sno++;
	            HSSFRow row = sheet.createRow((short) index);
	            
	            row.createCell((short) 0).setCellValue(sno);
	            row.createCell((short) 1).setCellValue(resultSet.getString("PhysicalAddrZip"));
	            row.createCell((short) 2).setCellValue(resultSet.getString("RowID"));
	            index++;
	            
            }
        	
            FileOutputStream fos = new FileOutputStream(tempFile);
            
            response.setHeader("Content-Type", "application/vnd.ms-excel");
            response.setHeader("Content-Transfer-Encoding", "binary");
            response.setHeader("Content-Length", String.valueOf(tempFile.length()));
            response.addHeader("Content-Disposition", String.format("attachment; filename=%s", tempFile.getName()));

            OutputStream outputStream = response.getOutputStream();
            FileInputStream fis = new FileInputStream(tempFile);
            
            try 
            {
                int n = 0;
                byte[] buffer = new byte[1024];
                while ((n = fis.read(buffer)) != -1) 
                {
                    outputStream.write(buffer, 0, n);
                }
                outputStream.flush();
            }
            finally {
                fis.close();
                tempFile.delete();
            }
            
            
            
            System.out.println("Final....");
        	
	 }
	
	catch(Exception e){
                    out.println(e);
               }
                finally {
                    if (resultSet!= null){ resultSet.close(); resultSet=null;}
                    if (con != null) {con.close();con = null;}
            }
 
}
%>

