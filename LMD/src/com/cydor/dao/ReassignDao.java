package com.cydor.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.cydor.bean.ReassignBean;
import com.cydor.util.DBConnection;

public class ReassignDao 
{

	Connection con = null;
	Statement statement = null;
	ResultSet resultSet = null;
	ResultSet resultSetcity = null;
	private int noOfRecords;

	
	public List<ReassignBean> list() throws SQLException 
	{
	  	List<ReassignBean> liststate = new ArrayList<>();
	  	try
		{
			con = DBConnection.createConnection(); //establishing connection
			statement = con.createStatement(); //Statement is used to write queries. Read more about it.
			resultSet = statement.executeQuery("Select State,City,Zip_Code from dev_cyd_leads.TerritoryLkup group by State order by State"); //Here table name is users and userName,password are columns. fetching all the records and storing in a resultSet.
	
			 while(resultSet.next()) 
			 {
				 ReassignBean category = new ReassignBean();
	             category.setState(resultSet.getString("State"));
	             category.setCity(resultSet.getString("City"));
	             category.setZip(resultSet.getString("Zip_Code"));
	             liststate.add(category);
	         }
			 
		}
		catch(SQLException e)
		{}	 
		 	return liststate;
		
	}
	
	public List<ReassignBean> campaignlist() throws SQLException 
	{
	  	List<ReassignBean> campaignlist = new ArrayList<>();
	  	try
		{
			con = DBConnection.createConnection(); //establishing connection
			statement = con.createStatement(); //Statement is used to write queries. Read more about it.
			resultSet = statement.executeQuery("SELECT distinct CC.Name,CAMPAIGN_UNIFIED_CODE__C FROM dev_cyd_leads.SF_CYDCOR_CAMPAIGNICLAGENTASSIGNMENT__C CIAA inner join dev_cyd_leads.SF_CYDCOR_CLIENTCAMPAIGN__C CC on CC.ID = CIAA.CLIENT_CAMPAIGN__C  where CC.ACTIVE__C = 1 and CIAA.ACTIVE_FLAG__C =1 "); //Here table name is users and userName,password are columns. fetching all the records and storing in a resultSet.
			 while(resultSet.next()) 
			 {
				 ReassignBean camplist = new ReassignBean();
				 camplist.setCampaignname(resultSet.getString("Name"));
				 camplist.setCampaigncode(resultSet.getString("CAMPAIGN_UNIFIED_CODE__C"));
				 campaignlist.add(camplist);
	         }
			 
		}
		catch(SQLException e)
		{}	 
		 	return campaignlist;
		
	}
	
	public List<ReassignBean> officelist() throws SQLException 
	{
	  	List<ReassignBean> officelists = new ArrayList<>();
		
	  	try
		{
			con = DBConnection.createConnection(); //establishing connection
			statement = con.createStatement(); //Statement is used to write queries. Read more about it.
			resultSet = statement.executeQuery("SELECT ICL_UNIFIED_CODE__C,NAME ICL_Name FROM dev_cyd_leads.SF_ICL_ACCOUNT  where IS_ACTIVE_IN_SF__C =1"); //Here table name is users and userName,password are columns. fetching all the records and storing in a resultSet.
			 while(resultSet.next()) 
			 {
				 ReassignBean offlist = new ReassignBean();
				 offlist.setOfficecode(resultSet.getString("ICL_UNIFIED_CODE__C"));
				 offlist.setOfficename(resultSet.getString("ICL_Name"));
				 officelists.add(offlist);
	         }
			 
		}
		catch(SQLException e)
		{}	 
		 	return officelists;
		
	}
	
	   //public List<ReassignBean> viewAllLeads(int offset,int noOfRecords)
	 	public List<ReassignBean> viewAllLeads() throws SQLException 
		{
	 		List<ReassignBean> viewAllLeads = new ArrayList<>();
	 		//String query = "SELECT Zip_Code FROM TerritoryLkup limit "+offset+","+noOfRecords+" ";
			try 
			{
				con = DBConnection.createConnection();
				statement = con.createStatement();
				resultSet = statement.executeQuery("SELECT Distinct Zip_Code FROM dev_cyd_leads.TerritoryLkup limit 46");
				while (resultSet.next()) 
				{
					ReassignBean terrtiorylist = new ReassignBean();
					terrtiorylist.setZip(resultSet.getString("Zip_Code"));
					viewAllLeads.add(terrtiorylist);
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			} 
			return viewAllLeads;
		}
	
	 	public List<ReassignBean> searchcity(String state) throws SQLException
	 	{
	 		List<ReassignBean> searchcity = new ArrayList<>();
	 		try 
			{
				con = DBConnection.createConnection();
				statement = con.createStatement();
				resultSet = statement.executeQuery("SELECT PhysicalAddrZip, count(RowID) as RowID FROM dev_cyd_leads_stg.stg_Quill_Leads where PhysicalAddrState= " + "'" + state  + "'" + " GROUP BY PhysicalAddrZip  ");
				while (resultSet.next()) 
				{
					ReassignBean terrtiorylist = new ReassignBean();
					terrtiorylist.setZip(resultSet.getString("PhysicalAddrZip"));
					searchcity.add(terrtiorylist);
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			} 
	 			return searchcity;
		
	 	}
	 	
	 	public List<ReassignBean> retrievestatesearch(String state,String city, String[] zip) throws SQLException 
		{
	 		List<ReassignBean> retrievestatesearch = new ArrayList<>();
	 		ReassignBean bean = new ReassignBean();
	 	    
			try 
			{
				con = DBConnection.createConnection();
				statement = con.createStatement();
				
				if(state != null && city==null && zip==null)
				{
					resultSet = statement.executeQuery("SELECT PhysicalAddrZip, count(RowID) as RowID FROM dev_cyd_leads_stg.stg_Quill_Leads where PhysicalAddrState= " + "'" + state  + "'" + "  and PhysicalAddrZip IS NOT NULL GROUP BY PhysicalAddrZip  ");
				}
				if(state != null && city != null && zip==null)
				{
					resultSet = statement.executeQuery("SELECT PhysicalAddrZip, count(RowID) as RowID FROM dev_cyd_leads_stg.stg_Quill_Leads where PhysicalAddrState = " + "'" + state  + "'" + " AND PhysicalAddrCity=" + "'" + city  + "'" + " and PhysicalAddrZip IS NOT NULL GROUP BY PhysicalAddrZip  ");
				}
				if(state != null && city != null && zip!= null)
				{
					resultSet = statement.executeQuery("SELECT PhysicalAddrZip, count(RowID) as RowID FROM dev_cyd_leads_stg.stg_Quill_Leads where PhysicalAddrState = = " + "'" + state  + "'" + " AND PhysicalAddrCity= " + "'" + city  + "'" + " AND PhysicalAddrZip PhysicalAddrZip IN (" + "'" + zip  + "'" + ") and PhysicalAddrZip IS NOT NULL GROUP BY PhysicalAddrZip ");
				}
				
				
				while (resultSet.next()) 
				{
					ReassignBean terrtiorylist = new ReassignBean();
					terrtiorylist.setZip(resultSet.getString("PhysicalAddrZip"));
					terrtiorylist.setRid(resultSet.getInt("RowID"));
					retrievestatesearch.add(terrtiorylist);
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			} 
			return retrievestatesearch;
		}
	
		

		
			 	
}
