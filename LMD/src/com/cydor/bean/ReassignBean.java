package com.cydor.bean;

public class ReassignBean
{
	
	public ReassignBean(String state,String city,String zip,String campaignname,String officecode,String campaigncode,String officename,String leadSource,String physicalAddrZip,int rid)
	{
		this.setState(state);
		this.setCity(city);
		this.setZip(zip);
		this.setCampaignname(campaignname);
		this.setOfficecode(officecode);
		this.setCampaigncode(campaigncode);
		this.setOfficename(officename);
		this.setLeadSource(leadSource);
		this.setPhysicalAddrZip(physicalAddrZip);
		this.setRid(rid);
	}
	public ReassignBean()
	{}
	
	private String state;
	private String city;
	private String zip;
	private String campaignname;
	private String officecode;
	private String officename;
	private String campaigncode;
	private String leadSource;
	private String physicalAddrZip;
	private int rid;
	
	
	
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	
	public String getPhysicalAddrZip() {
		return physicalAddrZip;
	}
	public void setPhysicalAddrZip(String physicalAddrZip) {
		this.physicalAddrZip = physicalAddrZip;
	}
	public void setLeadSource(String leadSource) 
	{
		this.leadSource = leadSource;
	}
	public String getLeadSource() 
	{
		return leadSource;
	}
	
	
	public void setOfficename(String officename) 
	{
		this.officename = officename;
	}
	public String getOfficename() 
	{
		return officename;
	}
	
	
	public void setCampaigncode(String campaigncode) 
	{
		this.campaigncode = campaigncode;
	}
	public String getCampaigncode() 
	{
		return campaigncode;
	}
	
	public void setCampaignname(String campaignname) 
	{
		this.campaignname = campaignname;
	}
	public String getCampaignname() 
	{
		return campaignname;
	}
	
	public void setOfficecode(String officecode) 
	{
		this.officecode = officecode;
	}
	public String getOfficecode() 
	{
		return officecode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	
	
	
	
}