package com.cydor.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;

import com.cydor.util.DBConnection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cydor.bean.ReassignBean;
import com.cydor.dao.ReassignDao;
import com.cydor.util.DBConnection;

/**
 * Servlet implementation class ReassignedServlet
 */
public class ReassignedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReassignedServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		ReassignDao dao = new ReassignDao();
		
		String city=request.getParameter("city");
		System.out.println(city);
		
		PrintWriter out1=response.getWriter();
        response.setContentType("text/xml");
    try
	{
		   
    		List<ReassignBean> liststate = dao.list();
			request.setAttribute("list_state", liststate);
			
			List<ReassignBean> campaginlist = dao.campaignlist();
			request.setAttribute("campaginlist", campaginlist);
			
			List<ReassignBean> officelists = dao.officelist();
			request.setAttribute("officelist", officelists);
			
			List<ReassignBean> viewAllLeads = dao.viewAllLeads();
			request.setAttribute("terrtiorylists", viewAllLeads);
				
			request.getRequestDispatcher("jsp/reassigned.jsp").forward(request, response);//RequestDispatcher is used to send the control to the invoked page.
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	
	public String downloadExcel( ) { //ServletOutputStream out){

        String outFileName = "BannedStudents.csv";
        int nRow = 1;
        String strQuery = null;
       
        try {

            // Getting connection here for mySQL database 
	        	Connection con = null;
	        	Statement statement = null;
	        	ResultSet resultSet = null;
	        	      
	        	con = DBConnection.createConnection(); //establishing connection
	    		statement = con.createStatement(); //Statement is used to write queries. Read more about it.
	    		
              // Database Query               
	    		strQuery = "select * from banned";
	    		Statement stmt=con.createStatement();

	    		ResultSet rs=stmt.executeQuery(strQuery);

              File file = new File(outFileName);
              FileWriter fstream = new FileWriter(file);
              BufferedWriter out = new BufferedWriter(fstream);

              //Get Titles
              String titleLine = "BannedID" + "," + "UserID" + "," + "AdminID"         
                   + "," + "Ban Start Date" + "," + 
                       "Ban End Date" + "," + "Penalty Count" + "," + 
                   "Description" + "," + "Status" + "\n";
              out.write(titleLine);

              //stmt = conn.createStatement();


              while (rs.next()) {
                  int BannedId = rs.getInt(1);
                  int UserID = rs.getInt(2); 
                  int AdminID = rs.getInt(3); 
                  String BanStartDate = rs.getString(4); 
                  String BanEndDate = rs.getString(5); 
                  int PenaltyCount = rs.getInt(6); 
                  String Description = rs.getString(7);                 
                  String Status = rs.getString(8);

                  String outline = BannedId + "," + UserID + "," + AdminID + 
                      "," + BanStartDate + "," + 
                      BanEndDate + "," + PenaltyCount + "," + Description +
                      "," + Status + "\n";

                  out.write(outline);

              } //end of while
              out.close();

      } catch (Exception e) {
          System.err.println("Got an exception!");
          System.err.println(e.getMessage());
      }
        return outFileName;
	
	}
}
