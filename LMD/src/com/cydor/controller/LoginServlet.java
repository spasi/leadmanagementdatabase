package com.cydor.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cydor.bean.LoginBean;
import com.cydor.dao.LoginDao;

public class LoginServlet extends HttpServlet 
{
	public LoginServlet() {}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String userName = request.getParameter("txtUserName");
		String password = request.getParameter("txtPassword");
		
		//LoginBean loginBean = new LoginBean(); //creating object for LoginBean class, which is a normal java class, contains just setters and getters. Bea classes are efficiently used in java to access user information wherever required in the application.
		//loginBean.setUserName(userName); //setting the username and password through the loginBean object then only you can get it in future.
		//loginBean.setPassword(password);
	
		//LoginDao loginDao = new LoginDao(); //creating object for LoginDao. This class contains main logic of the application.
		//String userValidate = loginDao.authenticateUser(loginBean); //Calling authenticateUser function
		
		//if(userValidate.equals("SUCCESS")) //If function returns success string then user will be rooted to Home page
		
		if(userName.equals("Sanjay") && password.equals("test123") || userName.equals("Satish") && password.equals("test123") || userName.equals("Swetha") && password.equals("test123"))
		{
			request.setAttribute("userName", userName); //with setAttribute() you can define a "key" and value pair so that you can get it in future using getAttribute("key")
			request.getRequestDispatcher("/ReassignedServlet").forward(request, response);//RequestDispatcher is used to send the control to the invoked page.
			//request.getRequestDispatcher("jsp/Home.jsp").forward(request, response);//RequestDispatcher is used to send the control to the invoked page.
			
		}
		else
		{
			request.setAttribute("errMessage", "Invalid username and password"); //If authenticateUser() function returnsother than SUCCESS string it will be sent to Login page again. Here the error message returned from function has been stored in a errMessage key.
		    request.getRequestDispatcher("jsp/Login.jsp").forward(request, response);//forwarding the request
		}
	}
}