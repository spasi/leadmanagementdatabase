package com.cydor.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection 
{
	public static Connection createConnection()
	{
		Connection con = null;
		String url = "jdbc:mysql://key-mysql1.cydcor.com:3306/dev_cyd_leads_stg"; 
		//url = "jdbc:mysql://key-mysql1.cydcor.com:3306/dev_cyd_leads_stg";
		String username = "test"; //MySQL username
		String password = "Cydcor123"; //MySQL password
		try 
		{
			try 
			{
				Class.forName("com.mysql.jdbc.Driver"); //loading mysql driver 
			} 
				catch (ClassNotFoundException e)
				{
					e.printStackTrace();
				} 
					con = DriverManager.getConnection(url, username, password); //attempting to connect to MySQL database
					System.out.println("Printing connection object "+con);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
			return con; 
	}
}